<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
</head>
<body>
    @foreach($products as $product)
        <p>id:{{ $product->getId() }}</p>
        <p>name:{{ $product->getName() }}</p>
        <p>price:{{ $product->getPrice() }}</p>
        <p>rating:{{ $product->getRating() }}</p>
        <p>image:{{ $product->getImageUrl() }}</p>
    @endforeach
</body>
</html>
