<?php

namespace App\Providers;

use App\Repository\ProductRepository;
use App\Repository\ProductRepositoryInterface;
use App\Services\ProductGenerator;
use Carbon\Laravel\ServiceProvider;

class ProductServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->app->bind(ProductRepositoryInterface::class, function () {
            return new ProductRepository(
                ProductGenerator::generate()
            );
        });
    }
}
