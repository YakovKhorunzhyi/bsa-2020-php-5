<?php

declare(strict_types=1);

namespace App\Entity;

class Product
{
    protected $id;

    protected $name;

    protected $price;

    protected $img;

    protected $rating;

    /**
     * Product constructor.
     *
     * @param $id
     * @param $name
     * @param $price
     * @param $url
     * @param $rating
     */
    public function __construct($id, $name, $price, $url, $rating)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->img = $url;
        $this->rating = $rating;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->img;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }
}
