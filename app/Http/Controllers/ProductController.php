<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;

class ProductController extends Controller
{
    public function getProducts()
    {
        return ProductArrayPresenter::presentCollection(
            (new GetAllProductsAction)->execute()->getProducts()
        );
    }

    public function getPopularProducts()
    {
        return (new GetMostPopularProductAction())->execute()->getProduct()->toArray();
    }

    public function getCheapProducts()
    {
        $products = (new GetCheapestProductsAction())->execute()->getProducts();

        return view('cheap_products', [
            'products' => $products,
        ]);
    }
}
