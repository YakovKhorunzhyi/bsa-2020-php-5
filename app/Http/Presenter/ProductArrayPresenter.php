<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Product;

class ProductArrayPresenter
{
    /**
     * @param Product[] $products
     *
     * @return array
     */
    public static function presentCollection(array $products): array
    {
        $productsAsArray = [];

        foreach ($products as $product) {
            $productsAsArray[] = $product->toArray();
        }

        return $productsAsArray;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    public static function present(Product $product): array
    {
        return $product->toArray();
    }
}
