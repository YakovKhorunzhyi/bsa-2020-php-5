<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;

class ProductGenerator
{
    const NUMBER_OF_PRODUCTS = 10;

    /**
     * @return Product[]
     */
    public static function generate(): array
    {
        $products = [];

        for ($i = 1; $i <= self::NUMBER_OF_PRODUCTS; ++$i) {

            $products[] = new Product(
                $i,
                'product' . $i,
                100.00 - rand(0, 80),
                'http://image-url' . $i . '.jpg',
                5 - rand(0, 4)
            );

        }

        return $products;
    }
}
