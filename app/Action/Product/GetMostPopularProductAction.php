<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetMostPopularProductAction
{
    public function execute(): GetMostPopularProductResponse
    {
        return new GetMostPopularProductResponse();
    }
}
