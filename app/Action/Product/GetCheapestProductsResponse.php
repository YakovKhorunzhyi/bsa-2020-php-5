<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsResponse
{
    const NUMBER_OF_PRODUCTS = 3;

    public function getProducts(): array
    {
        /** @var ProductRepositoryInterface $repository */
        $repository = app(ProductRepositoryInterface::class);
        $products = $repository->findAll();

        return $this->getCheapestProductSortedByPrice($products);
    }

    public function getCheapestProductSortedByPrice(array $products)
    {
        usort($products, function (Product $product1, Product $product2) {
            return $product2->getPrice() <=> $product1->getPrice();
        });

        $cheapestProducts = [];

        for ($i = 0; $i < self::NUMBER_OF_PRODUCTS; ++$i) {
            $cheapestProducts[] = array_pop($products);
        }

        return $cheapestProducts;
    }
}
