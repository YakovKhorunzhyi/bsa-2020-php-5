<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetAllProductsResponse
{
    public function getProducts(): array
    {
        return app(ProductRepositoryInterface::class)
            ->findAll();
    }
}
