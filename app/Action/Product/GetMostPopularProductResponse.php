<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductResponse
{
    public function getProduct(): Product
    {
        /** @var ProductRepositoryInterface $repository */
        $repository = app(ProductRepositoryInterface::class);

        $products = $repository->findAll();
        $popularProduct = null;

        foreach ($products as $product) {

            if (is_null($popularProduct)) {
                $popularProduct = $product;
                continue;
            }

            if ($product->getRating() > $popularProduct->getRating()) {
                $popularProduct = $product;
            }
        }

        return $popularProduct;
    }
}
