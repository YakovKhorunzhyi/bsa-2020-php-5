<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('products/cheap', 'ProductController@getCheapProducts')->name('cheap-products');
