<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api')
    ->get('products', 'ProductController@getProducts')
    ->name('products');

Route::middleware('api')
    ->get('products/popular', 'ProductController@getPopularProducts')
    ->name('popular-products');
